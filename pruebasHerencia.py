import unittest
from datetime import datetime
from herencia import *
#
class TestPersonas(unittest.TestCase):
    def setUp(self):
        Persona.total_personas = 0
        self.persona1 = Persona('Juan', 'Pérez', '01/01/2000', '12345678A')
        self.paciente1 = Paciente('Adri', 'Martinez', '06/05/1998', '596847363', ['bronquitis', 'muela de juicio'])
        self.medico1 = Medico('Dr. Martínez', 'Fernández', '03/03/1980', '13579246C', 'Pediatría')

    def test_presentacion(self):
        self.assertEqual(self.persona1.presentacion(), 'soy Juan')

    def test_presentacion_Medico(self):
        self.assertEqual(self.medico1.presentacion(),'soy Dr. Martínez de especialidad Pediatría')
    

    def test_ver_historial_medico(self):
        self.assertEqual(self.paciente1.ver_historial_medico(), "El paciente nombre = Adri, apellidos = Martinez, fecha de nacimiento = 06/05/1998, dni = 596847363 ha tenido los siguientes procesos: ['bronquitis', 'muela de juicio']")

    def test_consultar_agenda(self):
        self.assertEqual(self.medico1.consultar_agenda(), 'El medico Dr. Martínez , no tiene citas programadas')

    def test_modificar_cita(self):
        self.medico1.crear_consulta('Consulta con María')
        self.medico1.crear_consulta('Consulta con Juan')
        self.medico1.modificar_cita(2, 'Consulta con Pedro')
        self.assertEqual(self.medico1.consultar_agenda(), 'El medico Dr. Martínez tiene las siguientes citas programadas: Consulta con María,Consulta con Pedro')

    def test_ver_personascreadas(self):

     self.assertEqual(Persona.total_personas, 3)


    
    def test_eliminar_cita(self):
        
        self.medico1.crear_consulta('Consulta con María')
        self.medico1.crear_consulta('Consulta con Juan')
        self.medico1.eliminar_cita()
        self.assertEqual(self.medico1.consultar_agenda(), 'El medico Dr. Martínez tiene las siguientes citas programadas: Consulta con María')

    def test_modificar_cita_que_no_existe(self):
        # Agregamos una cita inicialmente
        self.medico1.crear_consulta('Consulta con María')
        
        # Intentamos modificar una cita que no existe (fuera de rango)
        with self.assertRaises(IndexError):
            self.medico1.modificar_cita(5, 'Consulta con Pedro')

        # Verificamos que la agenda no haya sido modificada
        self.assertEqual(self.medico1.consultar_agenda(), 'El medico Dr. Martínez tiene las siguientes citas programadas: Consulta con María')


if __name__ == '__main__':
    unittest.main()
