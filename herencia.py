from datetime import datetime

class Persona:
    """Clase que representa una Persona."""

    total_personas = 0  # Variable de clase para contabilizar el número total de personas

    def __init__(self, nombre, apellidos, fechanacimiento, dni):
        """
        Constructor de la clase Persona.

        :param nombre: nombre de la persona
        :param apellidos: apellidos de la persona
        :param fechanacimiento fecha de nacimiento de la persona
        :param dni: dni de la persona
        """
        self._nombre = nombre
        self._apellidos = apellidos
        self._fechanacimiento = fechanacimiento
        self._dni = dni
        Persona.total_personas += 1  # Incrementar el contador al crear una nueva instancia

    # Método getter para el nombre
    @property
    def nombre(self):
        return self._nombre

    # Método setter para el nombre
    @nombre.setter
    def nombre(self, nuevo_nombre):
        self._nombre = nuevo_nombre

    # Método getter para los apellidos
    @property
    def apellidos(self):
        return self._apellidos

    # Método setter para los apellidos
    @apellidos.setter
    def apellidos(self, nuevo_apellido):
        self._apellidos = nuevo_apellido

    # Método getter para la fecha de nacimiento
    @property
    def fechanacimiento(self):
        return self._fechanacimiento

    # Método setter para la fecha de nacimiento
    @fechanacimiento.setter
    def fechanacimiento(self, nueva_fechanacimiento):
        try:
            datetime.strptime(nueva_fechanacimiento, '%d/%m/%Y')
            self._fechanacimiento = nueva_fechanacimiento
        except ValueError:
            print("La fecha de nacimiento no tiene un formato válido. Utilice el formato dd/mm/aaaa")

    # Método getter para el dni
    @property
    def dni(self):
        return self._dni

    # Método setter para el dni
    @dni.setter
    def dni(self, nuevo_dni):
        self._dni = nuevo_dni

    @classmethod
    def obtener_total_personas(cls):
        """
        Método de clase para obtener el número total de personas creadas.
        """
        return cls.total_personas
    
    @classmethod
    def crear_persona(cls, nombre, apellidos, fechanacimiento, dni):
        """
        Método de clase que crea una nueva instancia de Persona.

        
        :return: Una nueva instancia de Persona.
        """
        return cls(nombre, apellidos, fechanacimiento, dni)
    def presentacion(self):
     """ 
     Metodo para ver como se sobreescribe un metodo con herencia
     """
     return f'soy {self._nombre}'


    def __str__(self):
        """
        Método para obtener una representación de la persona.
        """
        return f"nombre = {self._nombre}, apellidos = {self._apellidos}, fecha de nacimiento = {self._fechanacimiento}, dni = {self._dni}"


class Paciente(Persona):
    def __init__(self, nombre, apellidos, fechanacimiento, dni, historial_clinico):
        super().__init__(nombre, apellidos, fechanacimiento, dni)
        self.historial_clinico = historial_clinico

    def modificar_historial_clinico(self, historial_corregido):
        self.historial_clinico = historial_corregido

    def ver_historial_medico(self):
        return f"El paciente {super().__str__()} ha tenido los siguientes procesos: {self.historial_clinico}"


    @classmethod
    def crear_paciente(cls, nombre, apellidos, fechanacimiento, dni, historial_clinico):
        """
        Método de clase que crea una nueva instancia de Paciente.

        
        :return: Una nueva instancia de Paciente.
        """
        return cls(nombre, apellidos, fechanacimiento, dni, historial_clinico)
class Medico(Persona):
    def __init__(self, nombre, apellidos, fechanacimiento, dni, especialidad):
        super().__init__(nombre, apellidos, fechanacimiento, dni)
        self._especialidad = especialidad
        self._citas = []

    def consultar_agenda(self):
        if len(self._citas) == 0:
         return f'El medico {self._nombre} , no tiene citas programadas'
        else:
         return f"El medico {self.nombre} tiene las siguientes citas programadas: {','.join(self.citas)}"
    
    def modificar_cita(self,numero_cita,cita_nueva):
        self._citas[numero_cita - 1] = cita_nueva

    def eliminar_cita(self):
        self._citas.pop()
 
    def crear_consulta(self,consulta_nueva):
        self.citas.append(consulta_nueva)
    @property
    def citas(self):
        return self._citas
    @citas.setter
    def citas(self, n):
        self._citas = n

    def presentacion(self):
     """ 
     Metodo para ver como se sobreescribe un metodo con herencia
     """
     return f'soy {self._nombre} de especialidad {self._especialidad}'
        


Medico1 = Medico('dr Juan', 'Sanchez', '06/05/1998', '35746356D', 'medicocabecera')
Persona1 = Paciente.crear_paciente('Adri', 'Martinez', '06/05/1998', '596847363', ['bronquitis', 'muela de juicio'])

print(Medico1.consultar_agenda())
print(Persona1.ver_historial_medico())

Medico1.crear_consulta('a las 5 con maria')

print( Medico1.consultar_agenda())
#voy a modificar ahora el paciente y el médico
Medico1.eliminar_cita()
print( Medico1.consultar_agenda())

#paciente
Persona1._apellidos = 'Stegierski'

Persona1.historial_clinico = 'solo bronquitis'

print(Persona1.ver_historial_medico())

#medico / voy a modificar su agenda ya que se puede equivocar.
Medico1.crear_consulta('cita con kANDELA1')
Medico1.crear_consulta('cita con kANDELA2')

Medico1.modificar_cita(2,' mejor no hay nada')
print(Medico1.consultar_agenda())

print(f'En nuestro sistema hay un total de {Persona.obtener_total_personas()} personas')

Persona3 = Persona.crear_persona('walde', 'stegierski' ,'323232', '06/05/1998')

print(Persona3.presentacion())
print(Medico1.presentacion())